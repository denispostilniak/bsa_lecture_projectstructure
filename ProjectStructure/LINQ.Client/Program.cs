﻿using System;
using LINQ.Client.Services;

namespace LINQ.Client
{
    class Program
    {
        static void Main(string[] args) {
            string option;
            int parseOption = 0;
            int maxOptionValue = 8;
            int minOptionValue = 1;
            int id = 0;

            var clientLogicService = new ClientLogicService();

            while (true) {

                Console.WriteLine("Select task:\n" + "1\n" + "2\n" + "3\n" + "4\n" + "5\n" + "6\n" + "7\n");
                Console.WriteLine("Or\n 8: CRUD operations");

                Console.WriteLine("10: Exit\n");

                Console.WriteLine("Your choice: ");
                option = Console.ReadLine();
                try {
                    parseOption = Int32.Parse(option);
                } catch {
                    Console.WriteLine("Невірний вхідний формат! Спробуйте ще");
                    continue;
                }
                if (parseOption >= minOptionValue && parseOption <= maxOptionValue) {
                    switch (parseOption) {
                        case 1:
                            Console.WriteLine("Write user's id:");
                            try {
                               id = int.Parse(Console.ReadLine());
                            } catch(Exception e) {
                                Console.WriteLine(e.Message);
                                return;
                            }
                           var result1 = clientLogicService.GetTasksByUser(id);
                            Console.WriteLine("Result\t");
                            foreach(var item in result1) {
                                Console.WriteLine(item.Key + ": " + item.Value);
                            }
                            break;
                        case 2:
                            Console.WriteLine("Write user's id:");
                            try {
                                id = int.Parse(Console.ReadLine());
                            } catch (Exception e) {
                                Console.WriteLine(e.Message);
                                return;
                            }
                            var result2 = clientLogicService.GetTasksByUserCurrentYear(id);
                                foreach(var item in result2) {
                                Console.WriteLine(item.Name);
                            }
                            break;
                        case 3:
                            Console.WriteLine("Write user's id:");
                            try {
                                id = int.Parse(Console.ReadLine());
                            } catch (Exception e) {
                                Console.WriteLine(e.Message);
                                return;
                            }
                            var result3 = clientLogicService.GetTasksByUserNameCondition(id);
                            foreach(var item in result3) {
                                Console.WriteLine(item.Name);
                            }
                            break;
                        case 4:
                            var result4 = clientLogicService.GetTeamsOrderTenYears();
                            foreach(var item in result4) {
                                Console.WriteLine(item.Item2);
                                foreach(var subItem in item.Item3) {
                                    Console.WriteLine(subItem.FirstName + "\n");
                                }
                            }
                            break;
                        case 5:
                            var result5 = clientLogicService.GetUsersSortedByNameAndTasks();
                            foreach(var item in result5) {
                                Console.WriteLine(item.FirstName);
                            }
                            break;
                        case 6:
                            Console.WriteLine("Write user's id:");
                            try {
                                id = int.Parse(Console.ReadLine());
                            } catch (Exception e) {
                                Console.WriteLine(e.Message);
                                return;
                            }
                            var result6 = clientLogicService.GetLastProjectCountAndLongerTasks(id);
                            Console.WriteLine(result6.LastProject.Name + "," + result6.CountTasksPerLastProject);
                            break;
                        case 7:
                            var result7 = clientLogicService.GetProjectLogestAndShortestTaskAndUsersAmount();
                            foreach(var item in result7) {
                                Console.WriteLine(item.UsersAmountFilteredByProjectProperties + "," + item.Project.Name + "," + item.LongestTaskByDescription.Name);
                            }
                            break;
                        case 8:
                            CrudOperations();
                            break;
                        case 10:
                            return;
                    }

                } else {
                    Console.WriteLine("Values must be beetween 1 and 7");
                    continue;
                }
            } 
        }
            static void CrudOperations()
            {
                Console.WriteLine("Chose operation:\n 1: Create \n 2: Read \n 3: Update \n 4: Delete");
                var opt = Int32.Parse(Console.ReadLine());
                switch (opt)
                {
                case 1:
                    Console
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                }
            }
    }
}