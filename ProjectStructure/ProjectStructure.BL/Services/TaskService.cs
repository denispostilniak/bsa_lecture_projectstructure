﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.UnitOfWork.Interfaces;
using ProjectStructure.Common.Models.DTO;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<DAL.Models.Task> _repository;
        private readonly IUnitOfWork _unitOfWork;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper) {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.Set<DAL.Models.Task>();
        }
        public void CreateTask(TaskDTO taskDTO) {
            var task = _mapper.Map<Task>(taskDTO);

            _repository.Create(task);
            _unitOfWork.SaveChanges();
        }

        public void DeleteTask(int taskId) {
            _repository.Delete(taskId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TaskDTO> GetAllTasks() {
            var result = _repository.Get();
            return _mapper.Map<IEnumerable<TaskDTO>>(result);
        }

        public void UpdateTask(TaskDTO taskDTO) {
            var task = _mapper.Map<Task>(taskDTO);

            _repository.Update(task);
            _unitOfWork.SaveChanges();
        }
    }
}
