﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.UnitOfWork.Interfaces;
using ProjectStructure.Common.Models.DTO;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Project> _repository;
        private readonly IUnitOfWork _unitOfWork;
        public ProjectService(IMapper mapper, IUnitOfWork unitOfWork) {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.Set<Project>();
        }
        public void CreateProject(ProjectDTO projectDTO) {
            var project = _mapper.Map<Project>(projectDTO);

            _repository.Create(project);
            _unitOfWork.SaveChanges();
        }

        public void DeleteProject(int projectId) {
            _repository.Delete(projectId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<ProjectDTO> GetAllProjects() {
            var result = _repository.Get();
            return _mapper.Map<IEnumerable<ProjectDTO>>(result);
        }

        public void UpdateProject(ProjectDTO projectDTO) {
            var project = _mapper.Map<Project>(projectDTO);

            _repository.Update(project);
            _unitOfWork.SaveChanges();
        }
    }
}
