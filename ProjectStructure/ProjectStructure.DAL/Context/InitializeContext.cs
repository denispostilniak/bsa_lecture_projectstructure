﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Context
{
    public static class InitializeContext
    {
        public static async void Initialize(ProjectStructureContext context)
        {
            await TeamsInit( context);
            await UsersInit( context);
            await ProjectsInit( context);
            await TasksInit( context);
        }

        public static async Task ProjectsInit(ProjectStructureContext context)
        {
            if (!context.Projects.Any())
            {
                context.Projects.AddRange(new Models.Project
                {
                    Id = 1,
                    Name = "BSA",
                    Decription = "My new project on BSA",
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Now.AddDays(5),
                    AuthorId = 1,
                    TeamId = 1
                });
               await context.SaveChangesAsync();
            }
        }

        public static async Task TeamsInit(ProjectStructureContext context) {
            if (!context.Teams.Any()) {
                context.Teams.AddRange(new Models.Team {
                    Id = 1,
                    Name = "Command1",
                    CreatedAt = DateTime.Now
                });
                await context.SaveChangesAsync();
            }
        }

        public static async Task UsersInit(ProjectStructureContext context) {
            if (!context.Users.Any()) {
                context.Users.AddRange(new Models.User {
                    Id = 1,
                    FirstName = "Denis",
                    LastName = "Postilniak",
                    Email = "stuntdenko@gmail.com",
                    Birthday = DateTime.Parse("28.10.2000"),
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                });
                await context.SaveChangesAsync();
            }
        }

        public static async Task TasksInit(ProjectStructureContext context) {
            if (!context.Tasks.Any()) {
                context.Tasks.AddRange(new Models.Task {
                    Id = 1,
                    Name = "Add",
                    Description = "Add new logic",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now.AddDays(10),
                    ProjectId = 1,
                    PerfomerId = 1,
                    State = Models.TaskState.Created
                });
                await context.SaveChangesAsync();
            }
        }
    }
}
